% ========================================================================
%
% ECP 2020 annual meeting poster
%
% ========================================================================
% ---------------------------------- Class and Packages
% ----------------------------------

\documentclass[portrait,custom]{sciposter}

% ---------------------------------- Customization and Macros
% ----------------------------------

%\usepackage{times}

\usepackage{local}
%\usepackage{subfigure}
\usepackage{comment}
\usepackage[font=footnotesize,labelfont=bf]{caption}
  % Note: Font option above seems to do nothing. Problem with sciposter class or something else we've done in the poster?
  % If we want to change the font size in captions, we seem to have to manually do it for each caption.
\usepackage{rotating,multirow}
\usepackage{multicol}
%\bibliographystyle{unsrtnat}
\bibliographystyle{plainnat}
\usepackage[multidot]{grffile}
\usepackage{sectionbox}
\usepackage{url}
\usepackage[normalem]{ulem}
\usepackage{comment}
% Note: Add "showboxes" to textpos options when drafting the layout; this will 
% show the bounding box.
\usepackage[absolute,verbose,overlay]{textpos}

\usepackage{dcolumn}
\newcolumntype{d}{D{.}{.}{2.5}}
\newcolumntype{s}{D{.}{.}{1.0}}
\newcolumntype{i}{D{.}{.}{3.0}}


 \usepackage[T1]{fontenc}
 \usepackage[light,math]{iwona}
\renewcommand*\rmdefault{iwona}\normalfont\upshape
\usepackage{textcomp}
\usepackage{algorithmicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{tcolorbox}
\usepackage{textpos}

\usepackage{array}


% RTM: I'm defining a command for putting vectors in bold type: 
\renewcommand{\vec}[1]{\mathbf{#1}}
% RTM: And defining a vector norm command:
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}

% Colors for the pipelined Krylov material.
\definecolor{reductionCol}{rgb}{1.0,0.0,0.0} %red
\definecolor{neighborCol}{rgb}{0.95,0.4,0.0} %orange
\definecolor{localCol}{rgb}{0.0,0.6,0.0}     %green
\definecolor{pcCol}{rgb}{0.5,0.0,0.5}        %purple
\newcommand{\textRed}[1]{\textcolor{reductionCol}{#1}}
\newcommand{\textNei}[1]{\textcolor{neighborCol}{#1}}
\newcommand{\textLoc}[1]{\textcolor{localCol}{#1}}
\newcommand{\textPC}[1]{\textcolor{pcCol}{#1}}

\newcommand{\blue}[1]{\textcolor{Blue}{#1}}

% From user jfbu at 
% http://tex.stackexchange.com/questions/7219/how-to-vertically-center-two-images-next-to-each-other
% Makes it easy to do things like vertically align logos, etc.!
\newcommand*{\vcenteredhbox}[1]{\begingroup
\setbox0=\hbox{#1}\parbox{\wd0}{\box0}\endgroup}

\newcommand{\VERYHUGE}{\fontsize{72}{96}\selectfont}

\title{\VERYHUGE
Recent PETSc/TAO Enhancements for Exascale
} 

\author{%\vspace{-1cm} 
  %\VERYHUGE
  B.\ Smith (Lead PI), %$^{\alpha,\delta}$,
  R.T.\ Mills (co-PI), %$^{\beta,\delta}$,
  T.\ Munson (co-PI), %$^{\gamma,\delta}$,
  S.\ Wild (co-PI),
  M.\ Adams,
  S.\ Balay,
  G.\ Betrie,
  J.\ Brown,
  A.\ Dener,\linebreak[4]
  S.\ Hudson,
  M. Knepley,
  J.\ Larson,
  O.\ Marin,
  H.\ Morgan,
  J.-L.\ Navarro,
  K.\ Rupp,
  P.\ Sanan,
  H.\ Zhang,
  H.\ Zhang,
  J.\ Zhang\\
  %\vspace{-.6cm}
  % Affliations and roles go here, if there is space:
  %{\large $^\alpha$Director 
  %$^\beta$Deputy Director---Scientific Collaborations 
  %$^\gamma$Deputy Director---Applications
  %$^\delta$Argonne National Laboratory} 
}

% ========================================================================
\setmargins[3cm]

\begin{document}

\noleftlogo 
\norightlogo 
\vspace{-3in}
\conference{DOE Exascale Computing Project Fourth Annual Meeting, Houston, TX February 3--7, 2020.
\hfill
This research was supported by the Exascale Computing Project (17-SC-20-SC), 
a collaborative effort of the U.S. Department of Energy Office of Science 
and the National Nuclear Security Administration.
}
\maketitle
\vspace{-.5in}

% Place the logos using textpos
\setlength{\TPHorizModule}{\paperwidth}\setlength{\TPVertModule}{\paperheight}
% Right logo:
\begin{textblock}{0.05}[0.5,0.5](0.9,0.03)
\includegraphics[width=2.2in]{./logos/ECP.png}
\end{textblock}
% Left logo:
\begin{textblock}{0.1}[0.5,0.5](0.07,0.03)
\includegraphics[width=2.4in]{./logos/NNSA.png}
\begin{minipage}{2.6in}
\vspace{-.8in} % Stick things in minipage and put negative vspace here to get SC logo to align nicely.
\includegraphics[width=2.6in]{./logos/DOE_SC.pdf}
\end{minipage}
\end{textblock}


% Begin of Multicols-Enviroment
%
% - multcols* fills each column, leaving white space at the end
% - multcols  balances the columns, leaving white space at the bottom of
%   the page.
%
\begin{multicols}{3}

\section{Introduction}

PETSc/TAO is a software library for the scalable solution of linear,
nonlinear, and ODE/DAE systems, computation of adjoints (sometimes called
sensitivities) of ODE systems, and optimization. It has been developed 
and supported at ANL for more than 20 years, with other developers contributing 
from around the world. \\

Through ECP, we are working to extend the utility of PETSc/TAO for exascale-class
machines through both algorithmic improvements and close-to-the-hardware
performance optimizations.
We are also developing {\it libEnsemble}, a library to facilitate running ensembles of forward simulations
within a tight loop of optimization, sensitivity analysis, and uncertainty quantification.

\section{Enhancements to GPU Support and Experiences on Summit}

PETSc provides GPU support via CUSPARSE and ViennaCL:

\begin{center}
\renewcommand{\arraystretch}{1.2}
\begin{tabular}{|l|c|c|}
   \hline
                     & \textbf{CUDA/CUSPARSE}  & \textbf{ViennaCL} \\
   \hline
   Programming Model & CUDA                & CUDA/OpenCL/OpenMP \\
   \hline
   Operations        & Vector, MatMult     & Vector, MatMult \\
   \hline
   Matrix Formats    & CSR, ELL, HYB       & CSR \\
   \hline
   Preconditioners   & ILU0                & SA/Agg-AMG, Par-ILU0 \\
   \hline
   MPI-related       & Scatter             & - \\
   \hline
  \end{tabular}
\end{center}
~\\
Example using ViennaCL GPU implementation of Chow-Patel parallel ILU: \\
 \texttt{\small ./ex12 -vec\_type viennacl -mat\_type aijviennacl -pc\_type chowiluviennacl -m \$M -n \$N -log\_summary}
%~\\
%\begin{figure}
%\centering
%\includegraphics[width=0.6\textwidth]{figures/chow-ilu.png}
%\end{figure}

{Additional GPU Functionality:}
\begin{itemize}
\item OpenCL residual evaluation for PetscFE
\item GPU support for SuperLU-dist and SuiteSparse
\item GPU support for PETSc's native GAMG algebraic multigrid:
% RTM: I believe we don't need all the below details right here, so commenting out.
%  \begin{itemize}
%  \item Specify {\tt aijcusp} or {\tt aijviennacl} matrix types, then numerical setup and and solve phases
%  (Chebyshev/Jacobi smoothing, coarse grid restriction and interpolation) will run on GPU.
%  \end{itemize}
\end{itemize}

%%%%%%% Commenting this out
\begin{comment}
\begin{minipage}{0.6\columnwidth}
   \includegraphics[width=0.95\textwidth]{figures/gpu/elasticity-K20m-gmres}
\end{minipage}
\begin{minipage}{0.38\columnwidth}
   \begin{itemize}
    \item Performance gains up to 6x for GPU-friendly algorithms
    \item Small sweet spot for GPUs
    \item CPU better for small problems (strong scaling)
    \item Mathematically most efficient algorithms (e.g.~multigrid) often not GPU friendly
   \end{itemize}
\end{minipage}
\end{comment}
%%%%%%%

\subsection{Experiences on the Summit pre-exascale system}

\begin{itemize}
\item We have conducted detailed performance benchmarking and modeling studies for basic PETSc operations on OLCF's Summit,
which is the closest available proxy for anticipated exascale systems.
\item These studies provide a basis for heuristics to determine, for example, which levels of a multigrid
hierarchy should be executed on CPU vs.\ GPU.
\end{itemize}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\columnwidth]{figures/VecAXPY_CPU_vs_GPU.png}
\includegraphics[width=0.48\columnwidth]{figures/jed_VecAXPY_CPU_vs_GPU.png}
\end{center}
\caption{\small Effect of vector size on AXPY performance and memory throughput (one MPI rank per GPU) on a single node of the OLCF Summit system.
Note that as the vector sizes become large, the GPUs perform significantly better than the 42 Power9 CPU cores available, but the CPU cores
can be significantly faster for smaller vector sizes.
The figure on the right presents an alternative, {\it work-time spectrum} view of the data: in this view, both asymptotic bandwidth
and latency of the operations can be read directly from the figure.}
\end{figure}

\begin{table}[H]
\centering
\caption{\small Summary of PETSc vector operation performance on Summit, using a linear model in which time for data transfer is characterized by
a start-up latency $l$ and a bandwidth $b$ (subscripts $C$ and $G$ denote CPU and GPU, respectively).
In our linear model, the fraction of peak achieved will be $beta  = \frac{n}{l \times b}$.
{\it Small} vectors have $n = 10^3$--$10^5$ entries, {\it medium} $10^5$--$10^7$, and {\it large} $10^7$--$10^8$.
Latency is in $ 10^{-6}$ seconds, bandwidth in 8,000 Mbytes/second.
}
{\footnotesize \input{tables/summarytable.txt}}
\label{tab:summary}
\end{table}

\subsection{GAMG algebraic multigrid on GPUs}

\begin{minipage}{0.5\columnwidth}
\begin{center}
\includegraphics[width=\textwidth]{./figures/gamg_solver_time_breakdown.png}
\end{center}
\end{minipage}
\begin{minipage}{0.47\columnwidth}
\begin{itemize}
\item At left, breakdown of GAMG performance on SNES ex56 linear elasticity example on single Summit node.
\item PCApply: Smoothers and cycling through levels. Running fairly well on GPU.
\item GAMGGraph + GAMGCoarse: \\ Mesh setup; on CPU
\item MatPtAP: \\ Coarsened operator setup; on CPU
\end{itemize}
\end{minipage}

\begin{itemize}
%\item Measured PCApply time by level for geometric MG example: 6.5X GPU speedup, but lowest five levels are faster on CPU!
%Some speed to be gained by keeping these on CPU; adding controls in PETSc DM for this.
\item We are developing a hybrid (distributed+shared memory) parallel algorithm for finding maximal independent sets (used by GAMGCoarse).

\begin{minipage}{0.68\columnwidth}
  \begin{itemize}
    \item GAMG CPU implementation is targeted for distributed memory and is ill-suited for conversion to CUDA routines.
    \item AmgX approach difficult to use due to differences in decomposition across MPI ranks.
    \item ViennaCL algorithm exploits fine-grain parallelism but cannot be used directly on PETSc's parallel matrix.
  \end{itemize}
\end{minipage}
\begin{minipage}{0.25\columnwidth}
  \begin{center}
    \includegraphics[width=\textwidth]{./figures/aggregates.pdf}
    \caption{\small Example of aggregates (Bell et al., SISC 2012).}
  \end{center}
\end{minipage}
\end{itemize}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\columnwidth]{figures/mark_summit_1.png}\includegraphics[width=0.48\columnwidth]{figures/mark_summit_2.png}\\
\caption{\small Initial performance of GAMG on Summit at scale, running the SNES ex56 3D linear elasticity benchmark.
Preconditioner setup is performed on the CPU, while the multigrid solve happens on the GPU.
Speedup of GPU vs.\ CPU at scale is about 12.}
\end{center}
\end{figure}

\subsection {Fully Supporting MPI communication on GPU}
We recently unified implementations of the two communication modules in PETSc: \textit{VecScatter}, which
provides communication on PETSc vectors, and \textit{PetscSF}, which can do communication on any graph of any data type.

\begin{itemize}
\item Both VecScatter and PetscSF can communicate data on GPU, with or without GPU-aware MPI support.
\item Source data and destination data in communication do not need to be at the same place (i.e., host or device).
\item GPU-aware MPI (such as IBM Spectrum MPI, OpenMPI, MVAPICH2-GDR, etc.) gives better performance, since PETSc
does not need to copy data from GPU to CPU for communication purposes anymore.
\item PETSc overlaps local and remote communications, and also properly manages synchronization around MPI calls and
pack/unpack kernels.
\item Performance results on Summit with Spectrum MPI:
\begin{itemize}
  \item An MPI latency test (ping-pong)  written in PetscSF only adds 1$\mu$s overhead compared to OSU MPI
  microbenchmarks on GPUs.
  \item The sparse matrix vector multiplication of PETSc, i.e., MatMult, enjoys a 17\%
  performance improvement over regular MPI with 6 MPI ranks and 6 GPUs.
\end{itemize}

\newcolumntype{R}{>{\raggedleft\arraybackslash}m{4cm}}
% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[]
\caption{\small Total of 100 times of MatMult on one node of Summit. Matrix used is Florida HV15R, of size about 2M$\times$2M.
Three methods are
\textbf{(A)}: No CUDA-aware MPI, copy vector segment containing needed entries to CPU and then do
communication;
\textbf{(B)}: No CUDA-aware MPI, only copy needed vector entries to CPU then do communication;
\textbf{(C)}: CUDA-aware MPI.
}
\begin{tabular}{|l|R|R|R|R|}
	\hline
	  Configure &  Methods & \multicolumn{1}{p{5cm}|}{MatMult Time (Sec.)} & \multicolumn{1}{p{5cm}|}{CPU to GPU Copy (MB)}  & \multicolumn{1}{p{5cm}|}{GPU to CPU Copy (MB)} \\
  \hline
	\multirow{3}{3.5cm}{6 ranks +\\ 6 GPUs}  & A  & 0.130 &  102.0  &  269.0  \\ \cline{2-5}
	                                         & B  & 0.108 &  102.0  &  102.0  \\ \cline{2-5}
	                                         & C  & 0.089 &    0.0  &    0.0  \\ \hline
	 \multirow{3}{3.5cm}{24 ranks + \\ 6 GPUs} & A  & 0.096 &   46.1  &   67.2  \\ \cline{2-5}
	                                         & B  & 0.097 &   46.1  &   46.1  \\ \cline{2-5}
	                                         & C  & 0.097 &   0.0   &   0.0   \\ \hline
\end{tabular}
\end{table}

\end{itemize}

\section{libEnsemble: Coordinating Concurrent HPC Evaluations}

libEnsemble is a Python library to coordinate the concurrent evaluation of
dynamic ensembles of calculations. It is developed to use massively
parallel resources to accelerate the solution of design, decision, and
inference problems and to expand the class of problems that can benefit from
increased concurrency levels. Supports:

\begin{itemize}
  \item Extreme scaling
  \item Resilience/fault tolerance
  \item Monitoring/killing of jobs (and recovering resources)
  \item Portability and flexibility
  \item Exploitation of persistent data/control flow
\end{itemize}


\subsection{Overview}
libEnsemble is governed by three routines:
\begin{itemize}
  \item \texttt{gen\_f} Generates inputs to \texttt{sim\_f}
  \item \texttt{sim\_f} Evaluates a simulation or other evaluation based on output from \texttt{gen\_f}
  \item \texttt{alloc\_f} Decides whether \texttt{sim\_f} or \texttt{gen\_f} should be called (and with what input/resources) as workers become available
\end{itemize}

\begin{figure}
  \centering
  \includegraphics[width=0.4 \textwidth]{figures/libE/diagram_with_persis}
  \caption{\small Diagram of libEnsemble's manager/worker paradigm}
\end{figure}
Example \texttt{gen\_f}, \texttt{sim\_f}, \texttt{alloc\_f}, and calling scripts
are included.

\subsection{Details}
\begin{itemize}
  \item Employs a manager/worker communication using MPI, multiprocessing, or TCP
  \item Interfaces with user-provided executables, for example, 
    numerical simulations using considerable parallel resources.
  \item Controls and monitors calculations using different levels of resources,
    from subnode jobs to many-node simulations.
  \item Provides job controller to ensure that scripts
    are portable, resilient, and flexible when running on different systems.
  \item Enables automatic detection of compute resources on many platforms,
    including Theta, Cori, and Summit.
\end{itemize}

\subsection{Example use cases}

\begin{itemize}
  \item Identify multiple local optima for a \texttt{sim\_f}. libEnsemble can
    use the points from the APOSMM \texttt{gen\_f} to identify optima. After a
    point is ruled a local optimum, a different \texttt{gen\_f} can produce a
    collection of parameters necessary for sensitivity analysis of \texttt{sim\_f}.


  \item Evaluate a simulation \texttt{sim\_f} at a collection of parameter
  values, many of which cause the simulation to fail. libEnsemble can stop
  unresponsive evaluations and recover computational resources for future
  evaluations. The \texttt{gen\_f} can update the sampling after discovering
  regions where evaluations of \texttt{sim\_f} fail.
\end{itemize}


\begin{figure}
  \centering
  \includegraphics[width=0.41 \textwidth]{figures/libE/calcs_util_v_time}
  \includegraphics[width=0.41 \textwidth]{figures/libE/hist_completed_v_killed}
  \caption{\small Utilization graph and run-time histogram for a performance run of
  OPAL particle accelerator simulations with high failure rates. Run on 1024
  Theta/ALCF nodes}
\end{figure}


\section{Improved First-Order Optimization Methods}

\subsection{Unified Acceleration Framework for NCG and QN Algorithms}
  \begin{itemize}
    \item Diagonalized BFGS formula leveraged as preconditioners for nonlinear conjugate gradient (NCG) and Hessian initialization for quasi-Newton (QN) methods.
    \item Exploits mathematical connection between NCG and QN formulas through the Perry-Shanno scheme.
    \item Sieve algorithm automatically selects best default parameters based on extensive numerical experiments with CUTEst problems.
  \end{itemize}

  \begin{figure}
    \centering
    \begin{minipage}{0.49\textwidth}
      \includegraphics[width=\textwidth]{./figures/tao/fevals_cg_compare_versus}
    \end{minipage}
    \begin{minipage}{0.49\textwidth}
      \includegraphics[width=\textwidth]{./figures/tao/fevals_all_init_compare}
    \end{minipage}
    \caption{
      \small Comparison of NCG (left) and QN (right) methods with and without preconditioning/Hessian initialization.
    }
  \end{figure}

\subsection{Single NCG Algorithm, Many NCG Formulas}
  Preconditioning with diagonalized BFGS improves step direction scaling and eliminates reliance on specialized line 
  searches, allowing TAO to combine many NCG update formulas under a single algorithm using the More-Thuente line search.

  Available NCG updates:

  \begin{figure}
    \centering
    \begin{minipage}{0.49\textwidth}
      \begin{itemize}
        \item Preconditioned Gradient Descent
        \item Hestenes-Stiefel
        \item Fletcher-Reeves
        \item Polak-Ribiere-Polyak
        \item Modified Polak-Ribiere-Polyak
      \end{itemize}
    \end{minipage}
    \begin{minipage}{0.49\textwidth}
      \begin{itemize}
        \item Dai-Yuan
        \item Hager-Zhang
        \item Dai-Kou
        \item Kou-Dai
        \item Perry-Shanno (SSML-BFGS)
      \end{itemize}
    \end{minipage}
  \end{figure}

\section{Application Collaborations}

PETSc/TAO is being used by ten distinct software components of six ECP projects, and
also has interactions with five ECP co-design centers.
Below, we give some examples of how PETSc is being used.
We are always looking for new collaboration opportunities:
{\bf Please speak with us if you think PETSc may be of interest for your science.}

\subsection*{ADSE05-Subsurface}
\begin{itemize}
\item Chombo-Crunch using PETSc GAMG for solving Poisson and Helmholtz-type problems in complex flow domains,
      where geometric multigrid performs poorly
\item A PETSc interface for GEOSX flow and poroelasticity code is now in place.
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.45 \textwidth]{figures/subsurface/Calcite_pH_rectangle_OPT-1280x720.jpg}
\includegraphics[width=0.45 \textwidth]{figures/subsurface/FieldHydroFrac3DPlot.jpg}
\caption{\small Left: Chombo-Crunch simulated flow and pH inside a crushed calcite capillary experiment (Molins et al., Environ. Sci. Technol., 2014). Right: GEOS simulation of 3D distribution of fluid pressure and fracture aperture along a hydraulic fracture. \url{http://goo.gl/Vfd9tE}}
\end{figure}

\subsection*{ADSE12-WDMApp: Whole Device Modeling of Fusion Plasmas} %Whole device fusion modeling}
\begin{itemize}
\item XGC particle-in-cell code used to model edge region employs PETSc GAMG algebraic multigrid in electromagnetic field solver.
\item GENE (continuum code used to treat core region in toroidal reactor) uses PETSc + SLEPc for eigenvalue computations and
determination of maximum linear time step; Uses PETSc Krylov solvers and additive Schwarz to solve field equations.
\item M3D-C1 extended MHD code uses PETSc and SuperLU to solve linear systems in implicit time advance.
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{./figures/fusion/plasmafluc.png}
\includegraphics[width=0.45\textwidth]{./figures/fusion/genecode-webpage-graphic.jpg}
\caption{\small Left: Simulation of plasma within a tokamak. Green curve shows the magnetic
separatrix, which is included in the XGC grid. Image: C.S.\ Chang, Princeton Plasma Physics Laboratory
Right: GENE global gyrokinetic simulation of turbulence in the ASDEX Upgrade tokamak, performed using GENE. From \url{genecode.org}.}
\end{figure}

\subsection*{ADSE18-ExaStar: Exascale Models of Stellar Explosions}
\begin{minipage}{0.6\columnwidth}
\begin{itemize}
\item Poseidon, a new solver for the conformally flat metric approximation (CFA) to the Einstein field equations for use
within Chimera or other parts of the ExaStar framework, uses
PETSc single-level solvers for the five-component elliptical Jacobian systems arising from its Newton solver.
Enabling use of PETSc nonlinear solvers and multigrid schemes is planned.
\item FLASH5 multiphysics simulations can now utilize PETSc as a bottom-level solver through AMReX.
\end{itemize}
\end{minipage}
\begin{minipage}{0.33\columnwidth}
\begin{figure}
\includegraphics[width=\textwidth]{./figures/astro/exastar-chimera-core-collapse-cropped.png}
\caption{\small Entropy plot for Chimera simulation of core-collapse supernova (from ExaStar website).}
\end{figure}
\end{minipage}


% RTM: My thinking is that we probably don't need to waste space on a summary 
% in a small poster -- use the available space to put more actual content.
%\section{Summary and Future Directions}

\end{multicols}

\end{document}
